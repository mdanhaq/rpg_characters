﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attributes
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }

        public int Vitality { get; set; }

        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        // Overload the + operator for these custom types to simplify increasing attributes.
        public static PrimaryAttributes operator +(PrimaryAttributes x, PrimaryAttributes y)
        {
            PrimaryAttributes p_attributes = new PrimaryAttributes();
            p_attributes.Strength = x.Strength + y.Strength;
            p_attributes.Vitality = x.Vitality + y.Vitality;
            p_attributes.Dexterity = x.Dexterity + y.Dexterity;
            p_attributes.Intelligence = x.Intelligence + y.Intelligence;

            return p_attributes;
        }
    }
}
