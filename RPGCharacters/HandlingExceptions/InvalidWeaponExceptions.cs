﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.HandlingExceptions
{

    //Custom InvalidWeaponExceptions
    public class InvalidWeaponExceptions : Exception
        {
            public InvalidWeaponExceptions(string message) : base(message)
            {

            }
            public override string Message => "No possibility to equip that weapon";
        }
}


