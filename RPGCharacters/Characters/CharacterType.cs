﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters
{
    public enum CharacterType
    {
        Mage,
        Ranger, 
        Rogue,
        Warrior
    }
}
