﻿using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters
{
    public class Rogue : Character
    {
        ///<summary>
        ///Rogue has Dagger, Sword as weapon and Leather, Mail as armor
        ///</summary>
        public Rogue(String Name) : base(Name)
        {
            BasePrimaryAttributes.Vitality = 8;
            BasePrimaryAttributes.Strength = 2;
            BasePrimaryAttributes.Dexterity = 6;
            BasePrimaryAttributes.Intelligence = 1;
            ListedArmor = new List<ArmorTypes> { ArmorTypes.Leather, ArmorTypes.Mail };
            ListedWeapons = new List<WeaponTypes> { WeaponTypes.Daggers, WeaponTypes.Swords };
            CalculateSecondaryAttributes();
        }

        public override void getToUpperLevel(int level = 1)
        {
            base.getToUpperLevel(level);
            BasePrimaryAttributes.Vitality += 3;
            BasePrimaryAttributes.Strength += 1;
            BasePrimaryAttributes.Dexterity += 4;
            BasePrimaryAttributes.Intelligence += 1;
        }

        public override void CalculatingDamage()
        {
            
            double WeaponDPS = CalculatingWeaponDPS();
            Damage = WeaponDPS * (1 + (PrimaryAttributes.Dexterity / 100));
        }
    }
}
