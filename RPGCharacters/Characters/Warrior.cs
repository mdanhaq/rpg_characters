﻿using RPGCharacters.Attributes;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters
{
    public class Warrior : Character
    {
        ///<summary>
        ///Warrior has axe, swords, hammer as weapon and mail, plate as armor
        ///</summary>
        public Warrior (String Name) : base(Name)
        {
            BasePrimaryAttributes.Vitality = 10;
            BasePrimaryAttributes.Strength = 5;
            BasePrimaryAttributes.Dexterity = 2;
            BasePrimaryAttributes.Intelligence = 1;
            ListedArmor = new List<ArmorTypes> { ArmorTypes.Plate, ArmorTypes.Mail };
            ListedWeapons = new List<WeaponTypes> { WeaponTypes.Axes, WeaponTypes.Swords, WeaponTypes.Hammers };
            CalculateSecondaryAttributes();
        }

        public override void getToUpperLevel(int level = 1)
        {
            base.getToUpperLevel(level);
            BasePrimaryAttributes.Vitality += 5;
            BasePrimaryAttributes.Strength += 3;
            BasePrimaryAttributes.Dexterity += 2;
            BasePrimaryAttributes.Intelligence += 1;
            CalculateSecondaryAttributes();
        }

        public override void CalculatingDamage()
        {
            PrimaryAttributes totalAttributes = CalculatingTotalAttributes();
            double WeaponDPS = CalculatingWeaponDPS();
            Damage = WeaponDPS * (1 + (totalAttributes.Strength / 100));
        }

    }
}
