﻿using RPGCharacters.Attributes;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters
{
    public class Mage : Character
    {
        ///<summary>
        ///Mages has Staffs, Wands as weapon and Cloth as armor
        ///</summary>
        public Mage(String Name): base(Name)
        {
            BasePrimaryAttributes.Vitality = 5;
            BasePrimaryAttributes.Strength = 1;
            BasePrimaryAttributes.Dexterity = 1;
            BasePrimaryAttributes.Intelligence = 8;
            ListedArmor = new List<ArmorTypes> { ArmorTypes.Cloth };
            ListedWeapons = new List<WeaponTypes> { WeaponTypes.Staffs, WeaponTypes.Wands };
            CalculateSecondaryAttributes();
        }

        public override void getToUpperLevel(int level = 1)
        {
            base.getToUpperLevel(level);
            BasePrimaryAttributes.Vitality += 3;
            BasePrimaryAttributes.Strength += 1;
            BasePrimaryAttributes.Dexterity += 1;
            BasePrimaryAttributes.Intelligence += 5;
            CalculateSecondaryAttributes();
        }

        public override void CalculatingDamage()
        {
            PrimaryAttributes totalAttributes = CalculatingTotalAttributes();
            double WeaponDPS = CalculatingWeaponDPS();
            Damage = WeaponDPS * (1 + (totalAttributes.Intelligence/100));
        }
    }
}
