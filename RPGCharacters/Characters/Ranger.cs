﻿using RPGCharacters.Attributes;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters
{
    public class Ranger : Character
    {
        ///<summary>
        ///Rangers has Bow as weapon and Leather, Mail as armor
        ///</summary>
        public Ranger(String Name) : base(Name)
        {
            BasePrimaryAttributes.Vitality = 8;
            BasePrimaryAttributes.Strength = 1;
            BasePrimaryAttributes.Dexterity = 7;
            BasePrimaryAttributes.Intelligence = 1;
            ListedArmor = new List<ArmorTypes> { ArmorTypes.Leather, ArmorTypes.Mail };
            ListedWeapons = new List<WeaponTypes> { WeaponTypes.Bows };
            CalculateSecondaryAttributes();
        }

        public override void getToUpperLevel(int level = 1)
        {
            base.getToUpperLevel(level);
            BasePrimaryAttributes.Vitality += 2;
            BasePrimaryAttributes.Strength += 1;
            BasePrimaryAttributes.Dexterity += 5;
            BasePrimaryAttributes.Intelligence += 1;
        }

        public override void CalculatingDamage()
        {
            PrimaryAttributes totalAttributes = CalculatingTotalAttributes();
            double WeaponDPS = CalculatingWeaponDPS();
            Damage = WeaponDPS * (1 + (totalAttributes.Dexterity / 100));
        }

    }
}
