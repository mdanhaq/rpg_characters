﻿using RPGCharacters.Attributes;
using RPGCharacters.HandlingExceptions;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPGCharacters.Characters
{
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }

        public double Damage { get; set; }

        public PrimaryAttributes PrimaryAttributes { get; set; } = new PrimaryAttributes();
        public PrimaryAttributes BasePrimaryAttributes { get; set; } = new PrimaryAttributes();
        public SecondaryAttributes SecondaryAttributes { get; set; } = new SecondaryAttributes();
        //Certain characters can only equip specific types of weapons and armor
        public List<WeaponTypes> ListedWeapons { get; set; }
        public List<ArmorTypes> ListedArmor { get; set; }

        /// <summary>
        /// Storing the equipment as a Dictionary<Slot, Item>. 
        /// This ensures to have one item in each slot
        /// </summary>
        public Dictionary<SlotTypes, Item> Equipment { get; set; } = new Dictionary<SlotTypes, Item> 
        {
            { SlotTypes.Body, null },
            { SlotTypes.Legs, null },
            { SlotTypes.Head, null },
            { SlotTypes.Weapon, null },
        };

       ///<summary>
       ///When a character is created, they are provided a name &
       ///every character begins at level 1.
       ///</summary>
        public Character(string name)
        {
            this.Name = name;
            Level = 1;
        }
        
        ///<summary>
        ///Increase the level up
       ///Try to gain 0 or less levels, an Argument Exception should be thrown
       ///</summary>
        public virtual void getToUpperLevel(int level)
        {
            if (level <= 0)
            {
                throw new ArgumentException();
            }
            else
                Level += level;
        }

        ///<summary>
        ///If a character tries to equip a weapon they should not be able to, 
        ///either by it being the wrong type or being too high of a 
        ///level requirement, then a custom InvalidWeaponException should be thrown.
        ///</summary>
        public string EquippingWeapon(Weapon Weapon)
        {
            if (Weapon.ItemLevel > Level)
            {
                throw new InvalidWeaponExceptions("Need to level up to wear have this weapon");
            }

            if (!ListedWeapons.Contains(Weapon.WeaponTypes))
            {
                throw new InvalidWeaponExceptions("Weapon is not available");
            }

            Equipment[Weapon.ItemSlot] = Weapon;

            return "Equipped with a new weapon!";
        }

        ///<summary>
        ///If a character tries to equip a armor they should not be able to, 
        ///either by it being the wrong type or being too high of a 
        ///level requirement, then a custom InvalidArmorException should be thrown.
        ///</summary>
        public string EquippingArmor(Armor Armor)
        {
            if (Armor.ItemLevel > Level)
            {
                throw new InvalidArmorExceptions("Need to level up to wear have this armor");
            }

            if (!ListedArmor.Contains(Armor.ArmorTypes))
            {
                throw new InvalidArmorExceptions("Armor is not available");
            }

            Equipment[Armor.ItemSlot] = Armor;

            return "Equipped with a new armor!";
        }

        /// <summary>
        /// Secondary attributes calculation is done by as follows:
        /// characters health is calculated by multiplying the characters totalvitality with 10.
        /// characters armorRating is calculated by adding the totalstrength and totaldexterity 
        /// characters elemental resistance is equal to the intelligence.
        /// </summary>
        public void CalculateSecondaryAttributes()
        {
            PrimaryAttributes totalSecondary = CalculatingTotalAttributes();

            SecondaryAttributes.Health = totalSecondary.Vitality * 10;
            SecondaryAttributes.ArmorRating = totalSecondary.Strength + totalSecondary.Dexterity;
            SecondaryAttributes.ElementalResistance = totalSecondary.Intelligence;
        }

        /// <summary>
        /// The total attribute of the character is calculated by 
        /// the primaryattributes and the baseprimaryattributes.
        /// </summary>
        /// <returns>totalAttributes</returns>
        public PrimaryAttributes CalculatingTotalAttributes()
        {
            PrimaryAttributes primaryAttributes = CalculatingPrimaryAttributes();
            PrimaryAttributes totalAttributes = BasePrimaryAttributes + primaryAttributes;

            return totalAttributes;
        }

        /// <summary>
        /// Characters primary attributes calculation is based on the equipped armor.
        /// If an item is equipped in a slot, proceed to add the armor attributes to the calculatedStats object.
        /// </summary>
        public PrimaryAttributes CalculatingPrimaryAttributes()
        {
            PrimaryAttributes calculatingPrimaryStats = new PrimaryAttributes();
            List<Armor> armors = Equipment.Where(item => item.Key != SlotTypes.Weapon).Select(item => item.Value as Armor).ToList();

            foreach (Armor armor in armors)
            {
                if (armor != null)
                {
                    calculatingPrimaryStats += armor.ArmorAttributes;
                }
            }

            return calculatingPrimaryStats;
        }

        public abstract void CalculatingDamage();

        /// <summary>
        /// Weapon DPS is calculated by multiplying speed and damage. 
        /// If theres no weapon equipped the DPS is set to 1.
        /// </summary>
        public double CalculatingWeaponDPS()
        {
            double weaponDPS = 1;
            Weapon weapon = Equipment.Where(item => item.Key == SlotTypes.Weapon).Select(item => item.Value as Weapon).FirstOrDefault();

            if (weapon != null)
            {
                double weaponSpeed = weapon.WeaponAttributes.AttackSpeed;
                int weaponDamage = weapon.WeaponAttributes.Damage;
                weaponDPS = weaponDamage * weaponSpeed;
            }

            return weaponDPS;
        }
    }
}
