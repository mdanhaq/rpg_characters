﻿using RPGCharacters.Attributes;
using RPGCharacters.Characters;
//using RPGCharacters.Weapons;
//using RPGCharacters.Armors;
using System;
using System.Text;

namespace RPGCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior warriorAnsar = new Warrior("Ansar");

            warriorAnsar.CalculatingDamage();
            warriorAnsar.CalculateSecondaryAttributes();
            PrimaryAttributes totalAttributes = warriorAnsar.CalculatingTotalAttributes();

            /// <summary>
            /// Information Stats of the character is displayed in the console
            /// </summary>
            /// 
            StringBuilder characterStats = new StringBuilder();
            characterStats.AppendLine("Character stats: ");
            characterStats.AppendLine("Name: " + warriorAnsar.Name);
            characterStats.AppendLine("Level: " + warriorAnsar.Level);
            characterStats.AppendLine("Vitality: " + totalAttributes.Vitality);
            characterStats.AppendLine("Strength: " + totalAttributes.Strength);
            characterStats.AppendLine("Dexterity: " + totalAttributes.Dexterity);
            characterStats.AppendLine("Intelligence: " + totalAttributes.Intelligence);
            characterStats.AppendLine("Health: " + warriorAnsar.SecondaryAttributes.Health);
            characterStats.AppendLine("Armor Rating: " + warriorAnsar.SecondaryAttributes.ArmorRating);
            characterStats.AppendLine("Elemental Resistance: " + warriorAnsar.SecondaryAttributes.ElementalResistance);
            characterStats.AppendLine("DPS: " + warriorAnsar.Damage);

            Console.WriteLine(characterStats);
            
            Console.ReadLine();
        }
    }
}
