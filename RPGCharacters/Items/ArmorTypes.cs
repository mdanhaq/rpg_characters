﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
