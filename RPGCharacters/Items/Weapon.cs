﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacters.Attributes;

namespace RPGCharacters.Items
{
    public class Weapon : Item
    {
        public WeaponTypes WeaponTypes { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }
        public string ItemName { get; set; }

        public Weapon(){}
        public Weapon(string ItemName, int ItemLevel, SlotTypes ItemSlots, WeaponTypes weaponTypes, WeaponAttributes weaponAttributes): base(ItemName, ItemLevel, ItemSlots)
        {
            this.WeaponTypes = weaponTypes;
            this.WeaponAttributes = weaponAttributes;
        }
    }
}
