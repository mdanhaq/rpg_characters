﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    public abstract class Item
    {
        public string ItemsName { get; set; }
        public int ItemLevel { get; set; }
        public SlotTypes ItemSlot { get; set; }
        public Item(){}
        public Item(string ItemsName, int ItemLevel, SlotTypes ItemSlot)
        {
            this.ItemsName = ItemsName;
            this.ItemLevel = ItemLevel;
            this.ItemSlot = ItemSlot;
        }

    }
}
