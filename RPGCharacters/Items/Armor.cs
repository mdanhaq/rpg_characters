﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacters.Attributes;

namespace RPGCharacters.Items
{
    public class Armor : Item
    {
        public ArmorTypes ArmorTypes { get; set; }
        public PrimaryAttributes ArmorAttributes { get; set; }

        public Armor() { }

        public Armor(string ItemName, int ItemLevel, SlotTypes ItemSlot, ArmorTypes ArmorTypes, PrimaryAttributes ArmorAttributes) : base(ItemName, ItemLevel, ItemSlot)
        {
            this.ArmorTypes = ArmorTypes;
            this.ArmorAttributes = ArmorAttributes;
        }
    }
}
