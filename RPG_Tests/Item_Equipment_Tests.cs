﻿using RPGCharacters.Attributes;
using RPGCharacters.Characters;
using RPGCharacters.HandlingExceptions;
using RPGCharacters.Items;
using System;
using Xunit;

namespace RPG_Tests
{
    public class Item_Equipment_Tests
    {
        [Fact]

        //Weapon axe used for Warrior characters testing where 
        //warriors level is not up to the level of items level
        //So, it throws an invalid weapon exception
        public void EquippingWeapon_HighLevelWeapon_ThrowInvalidWeaponException()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = SlotTypes.Weapon,
                WeaponTypes = WeaponTypes.Axes,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Assert.Throws<InvalidWeaponExceptions>(() => warriorAnsar.EquippingWeapon(testAxe));
        }
        
        [Fact]
        //Armor plate slot body for Warrior characters testing where
        //warriors level is not up to the level of items level
        //so, it throws an invalid armor exception
        public void EquippingArmor_HighLevelArmor_ThrowInvalidArmorException()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            Armor testPlateBody = new Armor()
            {
                ItemsName = "Commone plate body armor",
                ItemLevel = 2,
                ItemSlot = SlotTypes.Body,
                ArmorTypes = ArmorTypes.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            Assert.Throws<InvalidArmorExceptions>(()=> warriorAnsar.EquippingArmor(testPlateBody));
        }

        //Armor plate slot body for Warrior characters testing where
        //warrior equips a valid weapon and returns with a success message
        [Fact]
        public void EquippingArmor_EquipValidArmor_ReturnSuccessMessage()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            Armor testPlateBody = new Armor()
            {
                ItemsName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = SlotTypes.Body,
                ArmorTypes = ArmorTypes.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            Assert.Equal("Equipped with a new armor!", warriorAnsar.EquippingArmor(testPlateBody));
        }


        //Equipping weapon such as axe for warrior character by issuing
        //a valid weapon and returns a success message 
        [Fact]
        public void EquippingWeapon_EquipValidWeapon_ReturnSuccessMessage()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            Weapon testAxe = new Weapon()
            {
                ItemsName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = SlotTypes.Weapon,
                WeaponTypes = WeaponTypes.Axes,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            Assert.Equal("Equipped with a new weapon!", warriorAnsar.EquippingWeapon(testAxe));

        }

        //Calculating DPS when no weapon is equipped for warrior character
        [Fact]
        public void CalculatingDamage_WithoutWeapon_ReturnDamage()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            warriorAnsar.CalculatingDamage();

            Assert.Equal(1 * (1 + (5 / 100)), warriorAnsar.Damage);
        }

    }
}
