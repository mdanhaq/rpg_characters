using RPGCharacters.Characters;
using System;
using Xunit;

namespace RPG_Tests
{
    public class Char_Attr_Level_Tests
    {

        
        [Fact]
        //A character is level 1 when created
        public void CreateCharacter_CharacterCreatedWithLevel_HaveLevelOne()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            Assert.Equal(1,warriorAnsar.Level);
        }

        [Fact]
        // When a character gains a level, it should be level 2
        public void GetToUpperLevel_CharacterGainsUpperLevel_ReturnsLevelTwo()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            warriorAnsar.getToUpperLevel();
            Assert.Equal(2, warriorAnsar.Level);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        // Try to gain 0 or less levels, an ArgumentException should be thrown.
        public void Level_GainLevelAsZeroOrLess_throwsArgumentException(int level)
        {
            Warrior warriorAnsar = new Warrior("Ansar");

           Assert.Throws<ArgumentException>(()=>warriorAnsar.getToUpperLevel(level));
        }

        [Fact]
        //Warrior character class is created with the proper default attributes
        public void CreateWarriorCharacter_BasePrimaryAttributesDeclaration_ReturnBasePrimaryAttributesOfWarrior()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            Assert.Equal(10, warriorAnsar.BasePrimaryAttributes.Vitality);
            Assert.Equal(5, warriorAnsar.BasePrimaryAttributes.Strength);
            Assert.Equal(2, warriorAnsar.BasePrimaryAttributes.Dexterity);
            Assert.Equal(1, warriorAnsar.BasePrimaryAttributes.Intelligence);
        }

        [Fact]
        //Mage character class is created with the proper default attributes
        public void CreateMageCharacter_BasePrimaryAttributesDeclaration_ReturnBasePrimaryAttributesOfMage()
        {
            Mage mageAnsar = new Mage("Ansar");
            Assert.Equal(5, mageAnsar.BasePrimaryAttributes.Vitality);
            Assert.Equal(1, mageAnsar.BasePrimaryAttributes.Strength);
            Assert.Equal(1, mageAnsar.BasePrimaryAttributes.Dexterity);
            Assert.Equal(8, mageAnsar.BasePrimaryAttributes.Intelligence);
        }

        [Fact]
        //Ranger character class is created with the proper default attributes
        public void CreateRangerCharacter_BasePrimaryAttributesDeclaration_ReturnBasePrimaryAttributesOfRanger()
        {
            Ranger rangerAnsar = new Ranger("Ansar");
            Assert.Equal(8, rangerAnsar.BasePrimaryAttributes.Vitality);
            Assert.Equal(1, rangerAnsar.BasePrimaryAttributes.Strength);
            Assert.Equal(7, rangerAnsar.BasePrimaryAttributes.Dexterity);
            Assert.Equal(1, rangerAnsar.BasePrimaryAttributes.Intelligence);
        }

        [Fact]
        //Rogue character class is created with the proper default attributes
        public void CreateRogueCharacter_BasePrimaryAttributesDeclaration_ReturnBasePrimaryAttributesOfRogue()
        {
            Rogue rogueAnsar = new Rogue("Ansar");
            Assert.Equal(8, rogueAnsar.BasePrimaryAttributes.Vitality);
            Assert.Equal(2, rogueAnsar.BasePrimaryAttributes.Strength);
            Assert.Equal(6, rogueAnsar.BasePrimaryAttributes.Dexterity);
            Assert.Equal(1, rogueAnsar.BasePrimaryAttributes.Intelligence);
        }

        [Fact]
        //Get Warrior characters level up, recalculate their primary attributes
        public void GetWarriorCharactersLevelUp_BasePrimaryAttributesIncreased_ReturnBasePrimaryAttributesOfWarrior()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            warriorAnsar.getToUpperLevel();
            Assert.Equal(15, warriorAnsar.BasePrimaryAttributes.Vitality);
            Assert.Equal(8, warriorAnsar.BasePrimaryAttributes.Strength);
            Assert.Equal(4, warriorAnsar.BasePrimaryAttributes.Dexterity);
            Assert.Equal(2, warriorAnsar.BasePrimaryAttributes.Intelligence);
        }

        [Fact]
        //Get Mage characters level up, recalculate their primary attributes
        public void GetMageCharactersLevelUp_BasePrimaryAttributesIncreased_ReturnBasePrimaryAttributesOfMage()
        {
            Mage mageAnsar = new Mage("Ansar");
            mageAnsar.getToUpperLevel();
            Assert.Equal(8, mageAnsar.BasePrimaryAttributes.Vitality);
            Assert.Equal(2, mageAnsar.BasePrimaryAttributes.Strength);
            Assert.Equal(2, mageAnsar.BasePrimaryAttributes.Dexterity);
            Assert.Equal(13, mageAnsar.BasePrimaryAttributes.Intelligence);
        }

        [Fact]
        //Get Rogue characters level up, recalculate their primary attributes
        public void GetRogueCharactersLevelUp_BasePrimaryAttributesIncreased_ReturnBasePrimaryAttributesOfRogue()
        {
            Rogue rogueAnsar = new Rogue("Ansar");
            rogueAnsar.getToUpperLevel();
            Assert.Equal(11, rogueAnsar.BasePrimaryAttributes.Vitality);
            Assert.Equal(3, rogueAnsar.BasePrimaryAttributes.Strength);
            Assert.Equal(10, rogueAnsar.BasePrimaryAttributes.Dexterity);
            Assert.Equal(2, rogueAnsar.BasePrimaryAttributes.Intelligence);
        }

        [Fact]
        //Get Ranger characters level up, recalculate their primary attributes
        public void GetRangerCharactersLevelUp_BasePrimaryAttributesIncreased_ReturnBasePrimaryAttributesOfRanger()
        {
            Ranger rangerAnsar = new Ranger("Ansar");
            rangerAnsar.getToUpperLevel();
            Assert.Equal(10, rangerAnsar.BasePrimaryAttributes.Vitality);
            Assert.Equal(2, rangerAnsar.BasePrimaryAttributes.Strength);
            Assert.Equal(12, rangerAnsar.BasePrimaryAttributes.Dexterity);
            Assert.Equal(2, rangerAnsar.BasePrimaryAttributes.Intelligence);
        }

        [Fact]
        public void CalculateSecondaryAttributes_SecondaryAttributesIncrease_ReturnSecondaryAttributes()
        {
            Warrior warriorAnsar = new Warrior("Ansar");
            warriorAnsar.getToUpperLevel();
            Assert.Equal(150, warriorAnsar.SecondaryAttributes.Health);
            Assert.Equal(12,warriorAnsar.SecondaryAttributes.ArmorRating);
            Assert.Equal(2, warriorAnsar.SecondaryAttributes.ElementalResistance);
        }
    }
}
